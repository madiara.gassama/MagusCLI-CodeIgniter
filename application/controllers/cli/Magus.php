<?php
/**
 * Librairie qui te permet de creer Controller, View, Model, Migrations, etc.
 * 
 * @author Madiara Gassama (Magus)
 * @link 
 * @copyright Copyright (c) 2018, Madiara Gassama 
 * @version 1.0.0
 */
class Magus extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		if ( ! $this->input->is_cli_request() ) {
			echo "This is command line interface tool.";
			exit();
			return false;
		}
		$this->load->helper('file');
	}


	/**
	 * Cette fonction permet de rediriger vers les differents fonction
	 * @param string $method
	 * @param array $params
	 * @return boolean
	 */
	public function _remap($method, $params = array())
	{
		switch ( $method ) {
			case 'controller':
				$this->controller(
					isset($params[0]) ? $params[0] : Null,
					isset($params[1]) ? $params[1] : 'CI_Controller'
				);
				break;
			case 'model':
				$this->model(
					isset($params[0]) ? $params[0] : Null,
					isset($params[1]) ? $params[1] : Null,
					isset($params[2]) ? $params[2] : Null,
					isset($params[3]) ? $params[3] : 'CI_Model'
				);
				break;
			case 'view':
				$this->view(	isset($params[0]) ? $params[0] : Null, $params);
				break;
			case 'migrations':
				$this->migrations(isset($params[0]) ? $params[0] : Null, $params);
				break;
			case 'migrate':
				$this->migrate();
				break;
			case 'version':
				$this->version();
				break;
			case 'server':
				$this->server();
				break;
			case 'cm':
			case 'all':
				if ( ! isset($params[0]) ) {
					echo "\n\033[33mUsage:\n\033[0m";
					echo " all name \n\n";
					break;
				}
				$this->controller($params[0], 'CI_Controller');
				$this->model($params[0], $params[0], $params[0].'Id', 'CI_Model');
				$this->view(	isset($params[0]) ? $params[0] : Null, $params);
				break;
			default:
				$this->index();
				break;
		}
		return true;
	}

	/**
	 * Cette fonction vas nous permettre de Remplir le contenu du Controller
	 * @param string $name
	 * @param string $extendsName
	 * @return string
	 */
	protected function _controller_creator($name, $extendsName)
	{
		$name = explode('/', $name)[count(explode('/', $name))-1]; // Find the end of array
		$data = "<?php\n\nclass ".$name." extends ".$extendsName." {\n";
		$data .= "\n	public function __construct()\n";
		$data .= "	{\n";
		$data .= "		parent::__construct();\n";
		$data .= "	}\n";

		$data .= "\n	function index()\n";
		$data .= "	{\n";
		$data .= "		".'$this->data'."['rows'] = ".'$this'."->".$name."_model->read();\n";
		$data .= "		".'$this->load'."->view('index',".'$data'.");\n";
		$data .= "	}\n";

		$data .= "\n	function add()\n";
		$data .= "	{\n";
		$data .= "		".'$data'." = array();\n";
		$data .= "		".'$this'."->Categories_model->add(".'$data'.");\n";
		$data .= "		".'$this->data'."['rows'] = ".'$this'."->".$name."_model->read();\n";
		$data .= "		".'$this->load'."->view('index',".'$data'.");\n";
		$data .= "	}\n";

		$data .= "\n	function edit(".'$id'." = '')\n";
		$data .= "	{\n";
		$data .= "		if(".'$this'."->uri->segment(3)){\n";
		$data .= "			".'$data'." = array();\n";
		$data .= "			".'$this'."->".$name."_model->edit(".'$id'.",".'$data'.");\n";
		$data .= "			".'$this->data'."['rows'] = ".'$this'."->".$name."_model->read();\n";
		$data .= "			".'$this->load'."->view('index',".'$data'.");\n";
		$data .= "		}else{\n";
		$data .= "			".'$this->data'."['rows'] = ".'$this'."->".$name."_model->read();\n";
		$data .= "			".'$this->load'."->view('index',".'$data'.");\n";
		$data .= "		}\n";
		$data .= "	}\n";

		$data .= "\n	function delete(".'$id'." = '')\n";
		$data .= "	{\n";
		$data .= "		if(".'$this'."->uri->segment(3)){\n";
		$data .= "			".'$data'." = array();\n";
		$data .= "			".'$this'."->".$name."_model->delete(".'$id'.",".'$data'.");\n";
		$data .= "			".'$this->data'."['rows'] = ".'$this'."->".$name."_model->read();\n";
		$data .= "			".'$this->load'."->view('index',".'$data'.");\n";
		$data .= "		}else{\n";
		$data .= "			".'$this->data'."['rows'] = ".'$this'."->".$name."_model->read();\n";
		$data .= "			".'$this->load'."->view('index',".'$data'.");\n";
		$data .= "		}\n";
		$data .= "	}\n";
		$data .= "}";
		return $data;
	}

	/**
	 * Cette fonction vas nous permettre de Remplir le contenu du Model
	 * @param string $name
	 * @param string $table
	 * @param integrer $primaryKey
	 * @param string $extendsName
	 * @return string
	 */
	protected function _model_creator($name, $table, $primaryKey, $extendsName)
	{
		$name = explode('/', $name)[count(explode('/', $name))-1]; // Find the end of array
		$data = "<?php\n\nclass ".$name."_model extends ".$extendsName." {\n";
		if ( isset($table) ) {
			$data .= '	protected $table = \''.$table."';\n";
		}
		if ( isset($primaryKey) ) {
			$data .= '	protected $primaryKey = \''.$primaryKey."';\n";
		}
		$data .= "\n	public function __construct()\n";
		$data .= "	{\n";
		$data .= "		parent::__construct();\n";
		$data .= "	}\n";
		
		$data .= "\n	function read()\n";
		$data .= "	{\n";
		$data .= "		".'$query = $this->db'."->get('".strtolower($name)."');\n";
		$data .= "		if(".'$query->'."num_rows() > 0){\n";
		$data .= "			foreach(".'$query->result() as $row'."){\n";
		$data .= "				".'$data[] = $row'.";\n";
		$data .= "			}\n";
		$data .= "			return ".'$data'.";\n";
		$data .= "		}\n";
		$data .= "	}\n";
		$data .= "\n";

		$data .= "\n	function add(".'$data'.")\n";
		$data .= "	{\n";
		$data .= "		if (".'$this->db'."->insert('".strtolower($name)."',".'$data)'.") { \n";
		$data .= "			return true;\n";
		$data .= "		}\n";
		$data .= "	}\n";

		$data .= "\n	function edit(".'$id,$data'.")\n";
		$data .= "	{\n";
		$data .= "		".'$this->db'."->where('Id',".'$id'."); \n";
		$data .= "		".'$this->db'."->update('".strtolower($name)."',".'$data'."); \n";
		$data .= "	}\n";

		$data .= "\n	function delete(".'$id'.")\n";
		$data .= "	{\n";
		$data .= "		".'$this->db'."->where('Id',".'$id'."); \n";
		$data .= "		".'$this->db'."->delete('".strtolower($name)."'); \n";
		$data .= "	}\n";
		$data .= "}";
		return $data;
	}

	/**
	 * Cette fonction vas nous permettre de remplir le contenu du View
	 * @param string $params
	 * @return string
	 */
	protected function _view_creator($params)
	{
		$data = '';
		foreach ($params as $key => $value) {
			$data .= '<?php $this->load->view(\''.str_replace('.','/', $value).'\') ?>'."\n";
		}
		return $data;
	}

	/**
	 * Cette fonction permet de remplir le contenu du fichier de migration
	 * @param string $name
	 * @return string
	 */
	protected function _migrations_creator($name)
	{
		$name = explode('/', $name)[count(explode('/', $name))-1]; // Nom du fichier de Migrations

		$data = "<?php\n\nclass Migration_".ucfirst($name)." extends CI_Migration {\n";
		$data .= "\n	public function up()\n";
		$data .= "	{\n";
		$data .= "		".'$this->dbforge'."->add_field(\n";
		$data .= "			array(\n";
		$data .= "				'id' => array(\n";
		$data .= "					'type' => 'INT',\n";
		$data .= "					'constraint' => 5,\n";
		$data .= "					'unsigned' => true,\n";
		$data .= "					'auto_increment' => true\n";
		$data .= "				),\n";
		$data .= "			)\n";
		$data .= "		);\n";
		$data .= "		".'$this->dbforge'."->add_key('id', TRUE);\n";
		$data .= "		".'$this->dbforge'."->create_table('".strtolower($name)."');\n";
		$data .= "	}\n";
		$data .= "\n	public function down()\n";
		$data .= "	{\n";
		$data .= "		".'$this->dbforge'."->drop_table('".strtolower($name)."');\n";
		$data .= "	}\n";
		$data .= "}";
		return $data;
	}

	// recursive create folder and return file path
	/**
	 * Cette fonction de creer le dossier et retourne le chemin du fichier
	 * @param string $fileName
	 * @param string $mvc
	 * @return array
	 */
	protected function _folder_creator($fileName, $mvc)
	{
		$folder = APPPATH.$mvc.'/';
		$arrDir = explode('.', $fileName); 
		unset($arrDir[count($arrDir)-1]);
		foreach ( $arrDir as $key => $value) {
			$folder .= $value.'/';
			if ( ! file_exists( $folder ) ) {
				mkdir( $folder );
			}
		}
		$arrDir = explode('.', $fileName);
		switch ( $mvc ) {
			case 'views':
				mkdir( $folder."".$fileName );
				//$arrDir[count($arrDir)-1] = strtolower( $arrDir[count($arrDir)-1] );
				$arrDir = array('add','edit','view');
				break;
			case 'migrations':
				$date = date("YmdHis");
				$arrDir[count($arrDir)-1] = $date."_".strtolower( $arrDir[count($arrDir)-1] );
				break;
			default:
				$arrDir[count($arrDir)-1] = ucfirst( $arrDir[count($arrDir)-1] );
				break;
		}
		if($mvc === 'views'){
			return $arrDir;
		}else{
			return implode('/', $arrDir);
		}
	}

	/**
	 * Cette fonction permet d'expliquer les commandes a l'utilisateur
	 * @return boolean
	 */
	public function index()
	{
		echo "\n\033[33mUsage:\n\033[0m";
		echo "- controller	Create controller\n";
		echo "- model		Create model\n";
		echo "- view		Create view\n";
		echo "- all		Create controller view and model\n\n";
		echo "- migrations	Create a Migrations\n";
		echo "- migrate	Execute our migrations\n\n";
		echo "- server	Run Server\n";
		echo "- version	Check version of CodeIgniter\n";
		return true;
	}

	/**
	 * Cette fonction permet d`expliquer comment marche les commandes pour le controller
	 * Et la creation du controller
	 * @param string $name
	 * @param string $extendsName
	 * @return boolean
	 */
	public function controller($name = Null, $extendsName = 'CI_Controller')
	{
		// No param, Will response help.
		if ( ! isset( $name ) ) {
			echo "\n\033[33mUsage:\n\033[0m";
			echo " controller name [extendsName]\n\n";
			echo "\033[33mArguments:\n\033[0m";
			echo " name		The name of the controller class (use . to seperate sub directory like last example)\n";
			echo " extendsName	This class extends which class\n\n";
			echo "\033[33mExample:\n\033[0m";
			echo " magus controller Test\n";
			echo " # Create a Test.php file in controllers folder.\n\n";
			echo " magus controller Test CI_Controller\n";
			echo " # Create a Test.php file and extends CI_Controller in controllers folder.\n\n";
			echo " magus controller hi.123.Test CI_Controller\n";
			echo " # Create a Test.php file and extends CI_Controller in controllers/hi/123 folder.\n\n";
			return false;
		}
		// Recursive create folder and return path
		$path = $this->_folder_creator($name, 'controllers');

		// File exist.
		if ( file_exists(APPPATH.'controllers/'.$path.'.php') ) {
			echo "This controller file already exists.\n";
			return false;
		}

		// Actually write file.
		if ( ! write_file(APPPATH.'controllers/'.$path.'.php',
											$this->_controller_creator($path, $extendsName) ) ) {
			echo "Unable to write the file.\n";
			return false;
		}
		echo $path . " controller was created!\n";
		return true;
	}

	/**
	 * Cette fonction permet d'expliquer comment marche les commandes pour le model
	 * Et la creation du model
	 * @param string $name
	 * @param string $table
	 * @param string $primaryKey
	 * @param $extendsName
	 * @return boolean
	 */
	public function model($name = Null, $table = Null, $primaryKey = Null, $extendsName = 'CI_Model')
	{
		// No param, Will response help.
		if ( ! isset( $name ) ) {
			echo "\n\033[33mUsage:\n\033[0m";
			echo " model name [table] [primaryKey] [extendsName] \n\n";
			echo "\033[33mArguments:\n\033[0m";
			echo " name		The name of the model class (use . to seperate sub directory like last example)\n";
			echo " table		This class will operate which table\n";
			echo " primaryKey	The primary key of table\n";
			echo " extendsName	This class extends which class\n\n";
			echo "\033[33mExample:\n\033[0m";
			echo " magus model Test\n";
			echo " # Create a file Test_model.php in models folder.\n\n";
			echo " magus model Product products\n";
			echo " # Create a file Product_model.php contain a variable \$table='products' in models folder.\n\n";
			echo " magus model User users user_id CI_Model\n";
			echo " # Create a file User_model.php contain 2 variable \$table='users' \$primaryKey='user_id' and extends CI_Model in models folder.\n\n";
			echo " magus model some.other.User users user_id CI_Model\n";
			echo " # Create a file User_model.php contain 2 variable \$table='users' \$primaryKey='user_id' and extends CI_Model in models/some.other folder.\n\n";

			return false;
		}
		// Recursive create folder and return path
		$path = $this->_folder_creator($name, 'models');

		// File exist.
		if ( file_exists(APPPATH.'models/'.$path.'_model.php') ) {
			echo "This model file already exists.\n";
			return false;
		}
		// Actually write file.
		if ( ! write_file(APPPATH.'models/'.$path.'_model.php',
											$this->_model_creator($path, $table, $primaryKey, $extendsName) ) ) {
			echo "Unable to write the file.\n";
			return false;
		}
		echo $path . " model was created!\n";
		return true;
	}

	/**
	 * Cette fonction permet d'expliquer comment marche les commandes pour la view
	 * Et la creation de la view
	 * @param string $name
	 * @param string $params
	 * @return boolean
	 */
	public function view($name = Null, $params = Null)
	{
		// No param, Will response help.
		if ( ! isset( $name ) ) {
			echo "\n\033[33mUsage:\n\033[0m";
			echo " view name [require_file] [require_file] [require_file] ...more\n\n";
			echo "\033[33mArguments:\n\033[0m";
			echo " name		The name of the view file be create (use . to seperate sub directory like last example)\n";
			echo " require_file	Assign will be required file in the view file (use . to seperate sub directory like last example)\n\n";
			echo "\033[33mExample:\n\033[0m";
			echo " magus view Test\n";
			echo " # Create a Test.php file in views folder.\n\n";
			echo " magus view some.Test\n";
			echo " # Create a Test.php file in views/some folder.\n\n";
			echo " magus view Test template.header\n";
			echo " # Create a Test.php file in views folder and this file contain views/template/header.php file.\n\n";
			echo " magus view user.index template.header template.footer \n";
			echo " # Create a index.php file in views/user folder and this file contain views/template/header.php and views/template/footer.php file.\n\n";
			return false;
		}
		// Recursive create folder and return path
		$path = $this->_folder_creator($name, 'views');

		// Remove the first param, in order to match the require file.
		unset($params[0]);

		// File exist.
		if ( file_exists(APPPATH.'views/'.$name.'/'.$path[0].'.php') ) {
			echo "This view file already exists.\n";
			return false;
		}
		if ( file_exists(APPPATH.'views/'.$name.'/'.$path[1].'.php') ) {
			echo "This view file already exists.\n";
			return false;
		}
		if ( file_exists(APPPATH.'views/'.$name.'/'.$path[2].'.php') ) {
			echo "This view file already exists.\n";
			return false;
		}

		// Actually write file.
		if ( ! write_file(APPPATH.'views/'.$name.'/'.$path[0].'.php',
											$this->_view_creator($params) ) ) {
			echo "Unable to write the file.\n";
			return false;
		}
		if ( ! write_file(APPPATH.'views/'.$name.'/'.$path[1].'.php',
											$this->_view_creator($params) ) ) {
			echo "Unable to write the file.\n";
			return false;
		}
		if ( ! write_file(APPPATH.'views/'.$name.'/'.$path[2].'.php',
											$this->_view_creator($params) ) ) {
			echo "Unable to write the file.\n";
			return false;
		}

		echo $name . " view was created!";
		return true;
	}


	/**
	 * Cette fonction permet d'expliquer comment marche les commandes pour la migrations
	 * Et la creation de la migrations
	 * @param string $name
	 * @return boolean
	 */
	public function migrations($name = Null)
	{
		// No param, Will response help.
		if ( ! isset( $name ) ) {
			echo "\n\033[33mUsage:\n\033[0m";
			echo " migrations name \n\n";
			echo "\033[33mArguments:\n\033[0m";
			echo " name		The name of the migrations\n";
			
			echo "\033[33mExample:\n\033[0m";
			echo " magus migrations Test\n";
			echo " # Create a file 2018117210640_Test.php in models folder.\n\n";

			return false;
		}
		// Recursive create folder and return path
		$path = $this->_folder_creator($name, 'migrations');
		
		// File exist.
		if ( file_exists(APPPATH.'migrations/'.$path.'.php') ) {
			echo "This migrations file already exists.\n";
			return false;
		}
		// Actually write file.
		if ( ! write_file(APPPATH.'migrations/'.$path.'.php',
											$this->_migrations_creator($name) ) ) {
			echo "Unable to write the file.\n";
			return false;
		}
		echo $path . " migrations was created!\n";
		return true;
	}

	/**
	 * Cette fonction permet de creer les tables de la migration au niveau de la 
	 * Base de donnee
	 * @return boolean
	 */
	public function migrate()
	{

		// load migration library
        $this->load->library('migration');

        if(!$this->migration->latest())
        {
            show_error($this->migration->error_string());
        } else {
            echo 'Migrations ran successfully!';
		}   
		
		return true;
	}

	/**
	 * Cette fonction permet de demarrer le serveur
	 * @return none
	 */
	public function server(){
		echo "
.##...##...####....####...##..##...####............####...##......######.
.###.###..##..##..##......##..##..##..............##..##..##........##...
.##.#.##..######..##.###..##..##...####...####....##......##........##...
.##...##..##..##..##..##..##..##......##..........##..##..##........##...
.##...##..##..##...####....####....####............####...######..######.
.........................................................................\n\n
Welcome to CodeIgniter Console 
-------------------------------------------------------------------------
built-in server is running in http://localhost:8766/ 
You can exit with `CTRL-C` \n";
		$verif = exec("php -S localhost:8766 " );
	}

	/**
	 * Cette fonction permet de creer les tables de la migration au niveau de la 
	 * Base de donnee
	 * @return boolean
	 */
	public function version()
	{

		// load migration library
        echo CI_VERSION;  
		
		return true;
	}

}