# Magus-CLI: Générateur de code CodeIgniter

Ce petit script vous permet de générer des contrôleurs, des modelés, des vues, des migrations, etc très facilement dans votre application basée sur codeigniter

## Installation

    $git clone https://gitlab.com/Magus005/MagusCLI-CodeIgniter.git

    Configuration Base de données application/config/database.php
        $db['default'] = array(
            'dsn'	=> '',
            'hostname' => 'localhost',
            'username' => '',
            'password' => '',
            'database' => '',
            'dbdriver' => 'mysqli',
            ...
        )

## Utilisation

### Aide
    $php index.php cli/magus help

### Créer un contrôleur vu modèle
    $php index.php cli/magus all posts

### Créer un contrôleur
    $php index.php cli/magus controlleur posts

### Créer un modèle
    $php index.php cli/magus model posts

### Créer un vue
    $php index.php cli/magus view posts

### Créer une migration
    $php index.php cli/magus migrations post

### Migrer la base de données
    $php index.php cli/magus migrate

### Démarrer le serveur
    $php index.php cli/magus server

### Savoir la version de CodeIgniter
    $php index.php cli/magus version

